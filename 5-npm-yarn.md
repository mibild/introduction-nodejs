# NPM

## Global vs. local

```bash
sudo npm install npm -g
```

```bash
npm install lodash
```

## Package.json

```bash
npm init
```

## Update

```bash
npm update
```

## Lock down dependency versions

```bash
npm shrinkwrap
```

## Publish to Repository

```bash
npm adduser
npm publish
```

## Ignore files

* add `.npmignore`

# YARN

* the "new" JS Package Manager
* Backward compatible
* `yarn install`
* `yarn add ...`
